﻿using NHibernate;
using NHibernate.Linq;
using Rd.Data.Helpers;
using RD.Domain.Entities;
using RD.Domain.Helpers;
using RD.Domain.Repositories;
using System.Linq;

namespace Rd.Data.Repositories
{
    public class Repository<T> : IRepository<T> where T : IEntity
    {
        private UnitOfWork _unitOfWork;
        public Repository(IUnitOfWork unitOfWork)
        {
            _unitOfWork = (UnitOfWork)unitOfWork;
        }

        protected ISession Session { get { return _unitOfWork.Session; } }

        public IQueryable<T> GetAll()
        {
            return Session.Query<T>();
        }

        public T GetById(int id)
        {
            return Session.Get<T>(id);
        }

        public void Create(T entity)
        {
            Session.Save(entity);
        }

        public void Update(T entity)
        {
            Session.Update(entity);
        }

        public void Delete(int id)
        {
            Session.Delete(Session.Load<T>(id));
        }

        public IQuery CreateQuery(string namedQuery)
        {
            return Session.GetNamedQuery(namedQuery);
        }
    }
}
