﻿using FluentNHibernate.Mapping;
using RD.Domain.Entities;

namespace Rd.Data.MappingOverrides
{
    public class DocumentMap : ClassMap<Document>
    {
        public DocumentMap()
        {
            Table("DbDocs.dbo.Document");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.Name).Not.Nullable();
            References(x => x.Author).Column("UserId");
            Map(x => x.CreationDate).Not.Nullable();
            Map(x => x.Path).Not.Nullable();
        }
    }
}
