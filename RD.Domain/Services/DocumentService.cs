﻿using RD.Domain.Entities;
using RD.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace RD.Domain.Services
{
    public interface IDocumentService
    {
        IList<Document> GetAll();
        Document GetById(int id);
        void Create(Document document);
        void Update(Document document);
        void Delete(int id);
        void AddDocument(HttpPostedFileBase upload, User currentUser);
        IList<Document> GetAllSortable();
        IList<Document> GetAllSortable(Func<Document, bool> predicate);
    }

    public class DocumentService : IDocumentService
    {
        private IRepository<Document> _documentRepository;

        public DocumentService(IRepository<Document> documentRepository)
        {
            _documentRepository = documentRepository;
        }

        public IList<Document> GetAll()
        {
            return _documentRepository
               .GetAll()
               .ToList();
        }

        public IList<Document> GetAllSortable()
        {
            return _documentRepository.GetAll()
                .OrderBy(x => x.Name)
                .OrderBy(x => x.CreationDate)
                .OrderBy(x => x.Author.UserName).ToList();
        }

        public IList<Document> GetAllSortable(Func<Document, bool> predicate)
        {
            return _documentRepository.GetAll()
               .OrderBy(x => x.Name)
               .OrderBy(x => x.CreationDate)
               .OrderBy(x => x.Author.UserName)
               .Where(predicate)
               .ToList();
        }

        public Document GetById(int id)
        {
            return _documentRepository.GetById(id);
        }

        public void Create(Document document)
        {
            _documentRepository.Create(document);
        }

        public void Update(Document document)
        {
            _documentRepository.Update(document);
        }

        public void Delete(int id)
        {
            _documentRepository.Delete(id);
        }



        public void AddDocument(HttpPostedFileBase upload, User currentUser)
        {
            string pathDirectoryFiles = HostingEnvironment.ApplicationPhysicalPath + "Files\\";
            string fileName = Path.GetFileName(upload.FileName);
            try
            {
                upload.SaveAs(Path.Combine(pathDirectoryFiles, fileName));

                var query = _documentRepository.CreateQuery("AddDocument");
                query.SetString("Name", Path.GetFileNameWithoutExtension(fileName));
                query.SetInt32("UserId", currentUser.Id);
                query.SetString("Path", Path.Combine(@"\Files\", fileName));
                query.ExecuteUpdate();
            }
            catch (Exception ex)
            {
                if (File.Exists(Path.Combine(pathDirectoryFiles, fileName)))
                {
                    File.Delete(Path.Combine(pathDirectoryFiles, fileName));
                }
            }
        }
    }
}
