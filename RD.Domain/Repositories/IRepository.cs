﻿using NHibernate;
using RD.Domain.Entities;
using System.Linq;

namespace RD.Domain.Repositories
{
    public interface IRepository<T> where T : IEntity
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Create(T entity);
        void Update(T entity);
        void Delete(int id);
        IQuery CreateQuery(string namedQuery);
    }
}
