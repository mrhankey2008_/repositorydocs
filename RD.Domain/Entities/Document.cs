﻿using System;
using System.IO;
using System.Web.Hosting;

namespace RD.Domain.Entities
{
    public class Document : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual DateTime CreationDate { get; set; }
        public virtual User Author { get; set; }
        public virtual string Path { get; set; }


        public virtual byte[] BinaryFile
        {
            get
            {
                string pathFile = HostingEnvironment.ApplicationPhysicalPath + Path;
                return File.ReadAllBytes(pathFile);
            }
            set { }
        }
    }
}
