﻿using Microsoft.AspNet.Identity;

namespace RD.Domain.Entities
{
    public class User : IUser<int>
    {
        public virtual int Id { get;  set; }
        public virtual string UserName { get; set; }
        public virtual byte[] PasswordHash { get; set; }
    }
}
