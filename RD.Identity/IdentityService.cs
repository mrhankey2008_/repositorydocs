﻿using RD.Domain.Entities;
using System.Threading.Tasks;

namespace RD.Identity
{
    public interface IIdentityService
    {
        Task CreateAsync(User user);
        Task DeleteAsync(User user);
        Task<User> FindByIdAsync(int userId);
        Task<User> FindByNameAsync(string userName);
        Task UpdateAsync(User user);
    }


    public class IdentityService : IIdentityService
    {
        private IdentityStore _identityStore;

        public IdentityService(IdentityStore identityStore)
        {
            _identityStore = identityStore;
        }

        public Task CreateAsync(User user)
        {
            return _identityStore.CreateAsync(user);
        }

        public Task DeleteAsync(User user)
        {
            return _identityStore.DeleteAsync(user);
        }

        public Task<User> FindByIdAsync(int userId)
        {
            return _identityStore.FindByIdAsync(userId);
        }

        public Task<User> FindByNameAsync(string userName)
        {
            return _identityStore.FindByNameAsync(userName);
        }


        public Task UpdateAsync(User user)
        {
            return _identityStore.UpdateAsync(user);
        }
    }
}
