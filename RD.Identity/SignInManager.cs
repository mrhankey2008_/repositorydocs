﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using RD.Domain.Entities;
using System.Security.Cryptography;
using System.Text;
using System.Linq;

namespace RD.Identity
{
    public class SignInManager : SignInManager<User, int>
    {

        public SignInManager(UserManager<User, int> userManager, IAuthenticationManager authenticationManager)
         : base(userManager, authenticationManager)
        {
        }

        public void SignOut()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
        }

        public override async Task<SignInStatus> PasswordSignInAsync(string userName, string password, bool isPersistent, bool shouldLockout)
        {
            return await CheckPassword(userName, password);
        }

        private async Task<SignInStatus> CheckPassword(string userName, string password)
        {
            var user = await UserManager.FindByNameAsync(userName);
            if (user == null)
                return SignInStatus.Failure;

            using (SHA512 sha512Hash = SHA512.Create())
            {
                if (VerifySha512Hash(sha512Hash, password, user.PasswordHash))
                {
                    await SignInAsync(user, false, true);
                    return SignInStatus.Success;
                }
                    
                else
                    return SignInStatus.Failure;
            }
        }

        private bool VerifySha512Hash(SHA512 sha512Hash, string passwordFromUI, byte[] hash)
        {
            var hashOfInput = sha512Hash.ComputeHash(Encoding.UTF8.GetBytes(passwordFromUI));

            if (hashOfInput.SequenceEqual(hash))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
