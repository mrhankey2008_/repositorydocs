﻿using RD.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RD.Identity
{
    public interface IIdentityStore
    {
        Task CreateAsync(User user);
        Task DeleteAsync(User user);
        Task<User> FindByIdAsync(int userId);
        Task<User> FindByNameAsync(string userName);
        Task UpdateAsync(User user);
    }
}
