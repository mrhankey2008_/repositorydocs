﻿using FluentNHibernate.Mapping;
using RD.Domain.Entities;

namespace RD.Identity
{
    public class UserMap : ClassMap<User>
    {
        public  UserMap()
        {
            Table("DbDocs.dbo.[User]");
            Id(x => x.Id).GeneratedBy.Identity();
            Map(x => x.UserName).Not.Nullable();
            Map(x => x.PasswordHash).Not.Nullable();
        }
    }
}
