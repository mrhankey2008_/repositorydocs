﻿using Microsoft.AspNet.Identity;
using RD.Domain.Entities;

namespace RD.Identity
{
    public class UserManager : UserManager<User,int>
    {
        public UserManager(IUserStore<User, int> store)
            :base(store)
        {
            UserValidator = new UserValidator<User, int>(this);
            PasswordValidator = new PasswordValidator();
        }
    }
}
