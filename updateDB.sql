CREATE TABLE [User]
(
 ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,  
 UserName NVARCHAR(40) NOT NULL,
 PasswordHash BINARY(64) NOT NULL
);

INSERT INTO dbo.[User] (UserName, PasswordHash)
        VALUES('admin', HASHBYTES('SHA2_512', 'password'));

CREATE TABLE Document
(
	ID int IDENTITY(1,1) NOT NULL PRIMARY KEY,
	Name NVARCHAR(255) NOT NULL,
	UserId int FOREIGN KEY REFERENCES [User](ID) NOT NULL,
	CreationDate DATETIME NULL,
	[Path] NVARCHAR(255) NOT NULL
);

CREATE PROCEDURE [dbo].[AddDocument]
    @Name nvarchar(255),
	@UserId int,
	@Path nvarchar(255)
AS
BEGIN

INSERT INTO [DbDocs].[dbo].[Document] (Name, UserId, CreationDate, [Path])
VALUES (@Name, @UserId, GETDATE(), @Path);
END
