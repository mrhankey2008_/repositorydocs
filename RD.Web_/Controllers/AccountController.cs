﻿using Microsoft.AspNet.Identity.Owin;
using RD.Identity;
using System.Web.Mvc;
using RD.Web_.Models.Identity;
using System.Web;

namespace RD.Web_.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            if(!User.Identity.IsAuthenticated)
            {
                return View();
            }
            return RedirectToAction("Index", "Document");
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                    var result = SignInManager.PasswordSignIn(model.UserName, model.Password, false, false);
                    if (result == SignInStatus.Success)
                    {
                        return RedirectToAction("Index", "Document");
                    }
                else
                {
                    ModelState.AddModelError("", "The user name or password provided is incorrect.");
                }
               
                
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult LogOff()
        {
            SignInManager.SignOut();
            return RedirectToAction("Login", "Account");
        }

        public SignInManager SignInManager
        {
            get { return HttpContext.GetOwinContext().Get<SignInManager>(); }
        }
        public UserManager UserManager
        {
            get { return HttpContext.GetOwinContext().GetUserManager<UserManager>(); }
        }

    }
}