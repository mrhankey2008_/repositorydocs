﻿using RD.Domain.Services;
using RD.Web_.Models.Document;
using System.Web;
using System.Web.Mvc;

namespace RD.Web_.Controllers
{
    [Authorize]
    public class DocumentController : BaseController
    {
        private IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        [HttpGet]
        public ActionResult Index(string searchString)
        {
            var viewModel = new IndexViewModel();
            viewModel.Documents = _documentService.GetAllSortable();

            if (!string.IsNullOrEmpty(searchString))
            {
                viewModel.Documents = _documentService.GetAllSortable(x => x.Name.Contains(searchString)
                || x.Author.UserName.Contains(searchString));
            
            }

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Upload(HttpPostedFileBase upload)
        {
            if (upload != null)
            {
                _documentService.AddDocument(upload, CurrentUser);
            }
            return RedirectToAction("Index");
        }

        [HttpGet]
        public FileResult DownloadFile(int idDoc)
        {
            var doc = _documentService.GetById(idDoc);
            string extension = System.IO.Path.GetExtension(doc.Path);
            return File(doc.BinaryFile, "application/" + extension, doc.Name + "." + extension);
        }
    }
}