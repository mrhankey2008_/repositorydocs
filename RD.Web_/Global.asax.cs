﻿using RD.Web_.App_Start;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;


namespace RD.Web_
{
    public class Global : HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            CreateFolderUploadFile();
        }

        private void CreateFolderUploadFile()
        {
            if (!System.IO.Directory.Exists(Server.MapPath(@"~/Files/")))
            {
                System.IO.Directory.CreateDirectory(Server.MapPath(@"~/Files/"));
            }
        }

    }
}