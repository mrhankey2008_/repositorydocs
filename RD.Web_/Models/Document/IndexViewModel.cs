﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RD.Web_.Models.Document
{
    public class IndexViewModel
    {
        public IList<Domain.Entities.Document> Documents { get; set; }
    }
}