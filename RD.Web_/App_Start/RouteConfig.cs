﻿using System.Web.Mvc;
using System.Web.Routing;

namespace RD.Web_
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

               routes.MapRoute(
                    name: null,
                    url: "{controller}/{action}",
                    defaults: new { controller = "Document", action = "Index" }
                );
        }
    }
}